// obj.hxx -- routines to handle loading scenery and building the plib
//            scene graph.
//
// Written by Curtis Olson, started October 1997.
//
// Copyright (C) 1997  Curtis L. Olson  - http://www.flightgear.org/~curt
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// $Id$


#ifndef _SG_OBJ_HXX
#define _SG_OBJ_HXX

#ifndef __cplusplus
# error This library requires C++
#endif

#include <simgear/compiler.h>

#include <string>

#include <osg/Node>
#include <osg/Group>

#include "SGOceanTile.hxx"

using std::string;

class SGBucket;
class SGMaterialLib;

// Generate an ocean tile
inline bool SGGenTile( const std::string&, const SGBucket& b,
                       SGMaterialLib *matlib, osg::Group* group )
{
  // Generate an ocean tile
  osg::Node* node = SGOceanTile(b, matlib);
  if (!node)
    return false;
  group->addChild(node);
  return true;
}

osg::Node*
SGLoadBTG(const std::string& path, SGMaterialLib *matlib, bool calc_lights, bool use_random_objects, bool use_random_vegetation);

#endif // _SG_OBJ_HXX
